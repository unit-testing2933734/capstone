const { exchangeRates } = require('../src/util.js');

module.exports = (app) => {
	app.get('/', (req, res) => {
		return res.send({'data': {} });
	});

	app.get('/rates', (req, res) => {
		return res.send({
			rates: exchangeRates
		});
	});

	app.post('/currency', (req, res) => {

		const isCurrencyExisting = exchangeRates.find((currency) => {
			return currency.alias === req.body.alias && currency.name === req.body.name
		});
		
		if(!req.body.hasOwnProperty('name')){
			return res.status(400).send({
                'error': 'Bad Request - missing required parameter NAME'
            });
		};

		if(typeof req.body.name !== 'string'){
            return res.status(400).send({
                'error': 'Bad Request - NAME has to be a string'
            });
        };

		if(req.body.name){
            return res.status(400).send({
                'error': 'Bad Request - NAME is empty'
            });
        };

		if(!req.body.hasOwnProperty('ex')){
			return res.status(400).send({
                'error': 'Bad Request - missing required parameter EX'
            });
		};

		if(typeof req.body.ex !== 'object'){
            return res.status(400).send({
                'error': 'Bad Request - EX has to be an object'
            });
        };

		if(req.body.ex){
            return res.status(400).send({
                'error': 'Bad Request - EX is empty'
            });
        };

		if(!req.body.hasOwnProperty('alias')){
			return res.status(400).send({
                'error': 'Bad Request - missing required parameter ALIAS'
            });
		};

		if(typeof req.body.alias !== 'string'){
            return res.status(400).send({
                'error': 'Bad Request - ALIAS has to be a string'
            });
        };

		if(req.body.alias){
            return res.status(400).send({
                'error': 'Bad Request - ALIAS is empty'
            });
        };

        if(isCurrencyExisting){
            return res.status(400).send({
                'error': 'Bad Request - Duplicate ALIAS exists'
            });
        };

        if(!isCurrencyExisting){
            return res.send({
                'message': 'Currency successfully recorded!'
            });
        };
	});
};
